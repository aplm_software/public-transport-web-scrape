#Esoteric function to construct link to the train timetable website

def construct_train_link(tt_code,version):
	# eg tt_code 02ALM
	out_str = 'https://ptv-prod-timetables.s3.amazonaws.com/timetables/'
	out_str += tt_code
	out_str += '/all/ttpdf-vic-'
	out_str += str(version) #letter val A-E or whatever...
	out_str += '-ttb-TIMINGPOINTS-'
	out_str += tt_code
	out_str += '--1542049835/timetable.pdf'
	return out_str