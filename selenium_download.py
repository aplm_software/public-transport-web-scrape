import os, time
from selenium import webdriver
from bs4 import BeautifulSoup



def download_file(pdf_url,out_fn):

	driver = webdriver.Firefox()

	#driver.get("https://ptv-prod-timetables.s3.amazonaws.com/timetables/02ALM/all/ttpdf-vic-G-ttb-TIMINGPOINTS-02ALM--1542049835/timetable.pdf")

	def get_request_session(driver):
		import requests
		session = requests.Session()
		for cookie in driver.get_cookies():
			session.cookies.set(cookie['name'], cookie['value'])

		return session



	url = pdf_url
	session = get_request_session(driver)
	r = session.get(url, stream=True)
	chunk_size = 2000
	with open(out_fn, 'wb') as file:
		for chunk in r.iter_content(chunk_size):
			file.write(chunk)


	driver.close()