import os
import pandas as pd
from tabula import read_pdf, convert_into

fns = os.listdir('.')


pdf_fns = [fn for fn in fns if '.pdf' in fn]


#now split up...

for pdf_fn in pdf_fns:


	print pdf_fn


	df =  read_pdf(pdf_fn)

	out_fn = pdf_fn.split('.pdf')[0] + '.csv'




	#df.to_csv('csv/' + out_fn, encoding='utf-8')

	convert_into('csv/' + out_fn, pdf_fn, format = '.csv')