#Esoteric function to construct link to the tram timetable website


def construct_tram_link(tt_code):
	out_str = 'https://ptv-prod-timetables.s3.amazonaws.com/timetables/'
	out_str += str(tt_code)
	out_str += '/all/ttpdf-vic- -ttb-TIMINGPOINTS-'
	out_str += str(tt_code) #letter val A-E or whatever...
	out_str += '--1542136484/timetable.pdf'
	return out_str

