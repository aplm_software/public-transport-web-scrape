
from reqs import *

from filter_url_by_str import *
from construct_train_link import *
from construct_tram_link import *

#tram timetables:

tt = open('tram_timetables.txt','r+')

trams=tt.read().splitlines()

for rel_tt in trams:

	headers = requests.utils.default_headers()
	headers.update({
	    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0',
	})

	red_link = requests.get(rel_tt, headers=headers)
	timetableLinkPage =BeautifulSoup(red_link.content)
	#print(timetableLinkPage)

	regex = re.compile(r'langsing/timetable/pdf')

	l=[]

	for a in timetableLinkPage.find_all('a', href=True):
		l.append(str(a['href']))

	selected_links = filter(regex.search, l)
	 
	#now rip out line
	line_code = filter_url_by_str('line=',selected_links)[0]
	url = construct_tram_link(line_code)

	try: 
		#must pause to stop server from blocking IP...
		time.sleep(15)
		print url
		out_fn = 'pdfs_trams/' + str(line_code) + '_' + '.pdf'
		download_file(url,out_fn) #to download using selenium
		
	except Exception as e:
		print e

	finally:
		print 'done'

