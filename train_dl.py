
from reqs import *

from filter_url_by_str import *
from construct_train_link import *
from construct_tram_link import *

tt = open('train_timetables.txt','r+')
trains=tt.read().splitlines()

for rel_tt in trains:


	headers = requests.utils.default_headers()
	headers.update({
	    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0',
	})

	red_link = requests.get(rel_tt, headers=headers)
	timetableLinkPage =BeautifulSoup(red_link.content)
	print(timetableLinkPage)

	regex = re.compile(r'langsing/timetable/pdf')

	l=[]

	for a in timetableLinkPage.find_all('a', href=True):
		l.append(str(a['href']))

	selected_links = filter(regex.search, l)
	 
	#now rip out line
	line_codes = filter_url_by_str('line=',selected_links)
	num_versions = filter_url_by_str('sup=',selected_links)

	out_links=[]

	for l,n in zip(line_codes, num_versions):
		link = construct_train_link(l,n)
		if link.find(' ')<0:
			print link	
			out_links.append(link)	

	#now try download....
	i=0
	for lc, url in zip(line_codes,out_links):
		i+=1
		out_fn = 'pdfs/' + str(lc) + '_' + str(i) + '.pdf'

		try: 
			time.sleep(15)
			print url
			out_fn = 'pdfs/' + str(lc) + '_' + str(i) + '.pdf'
			download_file(url,out_fn) #to download using selenium

		except Exception as e:
			print e

		finally:
			print 'up to: ' + str(i)

